var myApp = angular.module('myApp', []);

myApp.controller('newPlayerController', function($scope){
  
  $scope.name = "";
  
  $scope.age = 0;
  
  $scope.race = "";
  
  $scope.desc = "";
  
  $scope.points = 30;
  
  $scope.atkValue = 0;
  
  $scope.defValue = 0;
  
  $scope.spclValue = 0;
  
  $scope.pointStatus = ($scope.points > 0);
  
  
  $scope.updateAtk = function(){
    
    if($scope.points > 0){
    $scope.atkValue = (+$scope.atkValue + 1);
    
    $scope.points = (+$scope.points - 1);
    }
  }
  
  $scope.updateDef = function(){
    
    if($scope.points > 0){
    $scope.defValue = (+$scope.defValue + 1);
    
    $scope.points = (+$scope.points - 1);
    }
  }
  
  $scope.updateSpcl = function(){
    
    if($scope.points > 0){
    $scope.spclValue = (+$scope.spclValue + 1);
    
    $scope.points = (+$scope.points - 1);
    }
  }
});