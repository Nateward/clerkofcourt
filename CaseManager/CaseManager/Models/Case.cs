﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaseManager.Models
{
    public class Case
    {
        public int Id { get; set; }
        public string defendant { get; set; }
        public string prosecution { get; set; }
        public string defAddress { get; set; }
        public string defCity { get; set; }
        public string defState { get; set; }
        public int defZip { get; set; }
        public string caseSummary { get; set; }
    }
}