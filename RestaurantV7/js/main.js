
function loadErrors(){
	hideStuff();
}

function hideStuff(){
	var x = document.getElementsByClassName("errors");
	var submitBtn = document.getElementsByClassName("submit");
	for (i = 0; i < x.length; i++) {
			x[i].style.visibility = "hidden";
	}

	for (i = 0; i < submitBtn.length; i++) {
			submitBtn[i].style.display = "none";
	}
}


function theCheckBox() {
var checkBoxes = document.getElementsByClassName( 'dateChecker' );
var isChecked = false;
    for (var i = 0; i < checkBoxes.length; i++) {
        if ( checkBoxes[i].checked ) {
            isChecked = true;
        };
    };
    if ( isChecked ) {
        return true;
        } else {
            return false;
        }   
}


function required() {
	var test = false;
	var button = true;
	var name = document.forms["myForm"]["first"].value;
    var emailName = document.forms["myForm"]["email"].value;
    var phoneName = document.forms["myForm"]["phone"].value;
    var userReason = document.getElementById("reason");
    var	selection = userReason.options[userReason.selectedIndex].value;
    var info = document.forms["myForm"]["briefDescription"].value;
    var submitBtn = document.getElementsByClassName("submit");
    // var additional = document.forms["myForm"]["briefDescription"].value;
	
	while (button == true){
	    if (name == null || name == ""){
	        	document.getElementById("nameError").style.visibility = "visible";
	        	button	= false;
	    }
	    else{
	        	document.getElementById("nameError").style.visibility = "hidden";

	    }

	    if (emailName == null || emailName == "" || phoneName == null || phoneName == "") {
	        	document.getElementById("emailError").style.visibility = "visible";
	        	document.getElementById("phoneError").style.visibility = "visible";
	        	button = false;
	    }
	    else{
	        	document.getElementById("phoneError").style.visibility = "hidden";
	        	document.getElementById("emailError").style.visibility = "hidden";
	    }

	    if (selection == "other"){

	    	if (info == null || info == ""){
	        	document.getElementById("infoError").style.visibility = "visible";
	        	button = false;
	        }
	    	else{
	        	document.getElementById("infoError").style.visibility = "hidden";
	    	}
	    }

	    if (theCheckBox() == false){
	    	document.getElementById("checkError").style.visibility = "visible";
	        	button = false;
	    }
	    else {
	        document.getElementById("checkError").style.visibility = "hidden";
	        test = true
	    }

	  	if (test == true){
	  		var completeBtn = document.getElementsByClassName("complete");

	  		for (i = 0; i < completeBtn.length; i++) {
			completeBtn[i].style.display = "none";
			}

			for (i = 0; i < submitBtn.length; i++) {
			submitBtn[i].style.display = "block";
	}

	  		button = false;
	  	}
	 }
}