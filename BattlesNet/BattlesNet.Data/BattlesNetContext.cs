﻿using BattlesNet.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattlesNet.Data
{
    public class BattlesNetContext : DbContext
    {
        public DbSet<Character> Characters { get; set; }
        public DbSet<Battle> Battles { get; set; }
        public DbSet<CharacterStat> CharacterStats { get; set; }
        public DbSet<Race> Races { get; set; }
        public DbSet<Stat> Stats { get; set; }
        public BattlesNetContext():base("BattlesNetContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Battle>()
                        .HasRequired(m => m.Character)
                        .WithMany(t => t.Battles)
                        .HasForeignKey(m => m.CharacterId)
                        .WillCascadeOnDelete(false);
            modelBuilder.Entity<Battle>()
                       .HasRequired(m => m.Enemy)
                       .WithMany(t => t.EmenyBattles)
                       .HasForeignKey(m => m.EnemyId)
                       .WillCascadeOnDelete(false);
            
        }
    }
}
