﻿var myApp = angular.module('myApp', ['ngRoute']);

myApp.factory('casesFactory', function ($http) {
    var webAPIProvider = {};

    var url = '/api/Cases/';
  

    webAPIProvider.getCases = function () {
        return $http.get(url);
    };

    webAPIProvider.saveCase = function (caseFile) {
        return $http.post(url, caseFile);
    };

    webAPIProvider.saveEditedCase = function (urlId, caseFile) {
        return $http.put(url + urlId, caseFile);
    };

    return webAPIProvider;
});

myApp.config(function ($routeProvider) {
    $routeProvider
        .when('/CaseManager', {
            controller: 'CaseController',
            templateUrl: '/AngularViews/Index.html'
        })
        .when('/addCase', {
            controller: 'AddCaseController',
            templateUrl: '/AngularViews/addCase.html'
        })
        .when('/Case/Edit/:id', {
            controller: 'EditCaseController',
            templateUrl: '/AngularViews/editCase.html'
            
        })
        .when('/Case/Delete/:id', {
            controller: 'CaseController',
            templateUrl: '/AngularViews/Index.html'
        })
        .otherwise({ redirectTo: '/CaseManager' });
});

myApp.controller('CaseController', function ($scope, $location, $routeParams, $http, casesFactory) {
    casesFactory.getCases()
        .success(function (data) {
            $scope.cases = data;
        })
        .error(function (data, status) {
            alert('oh crap! status: ' + status);
        });

    $scope.deleteCase = function (Id) {
        var urlAndId = ('/api/Cases/' + Id);
        $http.delete(urlAndId)
    .success(function (response) { $scope.caseFile = response; });
    };
});

myApp.controller('AddCaseController', function ($scope, $location, casesFactory) {
    $scope.caseFile = {};

    $scope.save = function () {
        casesFactory.saveCase($scope.caseFile)
            .success(function () {
                $location.path('/CaseManager');
            })
            .error(function (data, status) {
                alert('oh crap! status: ' + status);
            });
    };
});

myApp.controller('EditCaseController', ['$scope', '$http', '$routeParams', '$location', function ($scope, $http, $routeParams, $location, casesFactory) {

        $scope.caseFile = {};

        $http.get('/api/cases/' + $routeParams.id)
        .success(function (response) { $scope.caseFile = response; });

       $scope.updateCase = function () {

           var Id = $scope.caseFile.Id;
           var urlAndId = ('/api/Cases/' + Id);

            $http.put(urlAndId, $scope.caseFile)
            .success(function () {
                $location.path('/CaseManager');
            })
            .error(function (data, status) {
                alert('oh crap! status: ' + status);
            });
    };
}]);

myApp.controller('DeleteCaseController', ['$scope', '$http', '$routeParams', '$location', function ($scope, $http, $routeParams, $location, casesFactory) {

    $http.get('/api/cases/' + $routeParams.id)
        .success(function (response) { $scope.caseFile = response; });
}]);