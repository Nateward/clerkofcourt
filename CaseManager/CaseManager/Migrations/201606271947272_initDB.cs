namespace CaseManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cases",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        defendant = c.String(),
                        prosecution = c.String(),
                        defAddress = c.String(),
                        defCity = c.String(),
                        defState = c.String(),
                        defZip = c.Int(nullable: false),
                        caseSummary = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Cases");
        }
    }
}
