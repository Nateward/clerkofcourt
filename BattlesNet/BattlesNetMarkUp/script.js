// Code goes here
function loadCharacterData (callback){
  $.ajax({
    method:'GET',
    url:'characters.json',
    dataType:'JSON',
    success:callback
    
  });
}
function DrawCharacterTable(characters){
  $.each(characters,function(index,character){
  var row = "<tr><td><a href='#'>"+character.Name+"</a></td><td>"+character.Race+"</td><td><a href='edit.html?CharacterID="+character.Id +"'>Edit</a> | <a href='#'>Delete</a></td></tr>";
  $("#characters tbody").append(row);
  })
  
}
$(document).ready(function(){
  loadCharacterData(DrawCharacterTable);
});